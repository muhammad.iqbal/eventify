import React, { Component } from 'react';
import { Image,View,Drawer } from 'react-native';
import { Container, Button, Text, Header, Left, Icon, Right, Body, Title, Tabs, Tab, ScrollableTab, TabHeading, Card, CardItem, Thumbnail, H1,Fab } from 'native-base';

export default class App extends Component {
  render() {
    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Singapore Events</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name='refresh' />
            </Button>
          </Right>
        </Header>

        



        <Tabs>
          <Tab heading={<TabHeading><Icon name="thumbs-up" /><Text>INTERESTS</Text></TabHeading>}>
            <Card>

              <CardItem cardBody>
                <Image source={{ uri: 'https://perureports.com/wp-content/uploads/2018/02/travel-2.jpg' }} style={{ height: 200, width: null, flex: 1 }} />
              </CardItem>

              <CardItem>
                <H1>Dummy Text</H1>
              </CardItem>


              <CardItem>

                <Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap </Text>

              </CardItem>


              <CardItem>
                <Left>
                  <Button transparent>
                    <Icon active name="ios-more" />
                  </Button>
                </Left>

                <Right>
                  <Button transparent>
                    <Text style={{color:'#FF00FF'}}>More Detail</Text>
                  </Button>
                </Right>
              </CardItem>
            </Card>
          </Tab>
          <Tab heading={<TabHeading><Icon name="star" /><Text>FEATURED</Text></TabHeading>}>
            <Text>INTERESTS</Text>
          </Tab>
          <Tab heading={<TabHeading><Icon name="ios-pin" /><Text>NEARBY</Text></TabHeading>}>
            <Text>INTERESTS</Text>
          </Tab>
        </Tabs>




        <View >
          <Fab
            active={true}

            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => alert("Clicked")}>
            <Icon name="add" />
          </Fab>
        </View>
      </Container>
    );
  }
}